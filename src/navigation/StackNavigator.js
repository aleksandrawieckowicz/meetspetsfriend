// StackNavigator.js
import React from 'react';
import { View, Text, Image } from 'react-native';
import { createStackNavigator } from '@react-navigation/stack';
import HomeScreen from '../screens/HomeScreen';
import PetsFriendsScreen from '../screens/PetsFriendsScreen';
import FriendInfoScreen from '../screens/FriendInfoScreen';
import AddPetScreen from '../screens/AddPetScreen';
import AddFriendScreen from '../screens/AddFriendScreen';
import FriendTabsNavigator from './FriendTabsNavigator';
import AllFriendMapScreen from '../screens/AllFriendMapScreen';
import ChatScreen from '../screens/ChatScreen';
import colors from '../styles/colors';
import LogoTitle from '../components/molecules/LogoTitle';

const Stack = createStackNavigator();

const StackNavigator = () => {
  return (
    <Stack.Navigator initialRouteName="Home"  screenOptions={{
      headerStyle: {
        backgroundColor: colors.mainColor,
        height: 120, // Wysokość nagłówka (możesz dostosować wartość według potrzeb)
      },
    }}>
      <Stack.Screen name="Home" component={HomeScreen} options={{ headerTitle: (props) => <LogoTitle {...props} /> }}  />
      <Stack.Screen name="AllFriendMapScreen" component={AllFriendMapScreen} options={{ headerTitle: (props) => <LogoTitle {...props} fontSize={24} title="Zarządzaj spacerem" /> }} />
      <Stack.Screen name="PetsFriendsScreen" component={PetsFriendsScreen} options={{ headerTitle: (props) => <LogoTitle {...props} fontSize={22} title="Sprawdź informacje o&nbsp;przyjacielu" /> }}/>
      <Stack.Screen name="FriendTabsNavigator" component={FriendTabsNavigator} options={{ headerTitle: (props) => <LogoTitle {...props} fontSize={26} title="Twój przyjaciel" /> }} />
      <Stack.Screen name="AddPetScreen" component={AddPetScreen} options={{ headerTitle: (props) => <LogoTitle {...props} fontSize={26} title="Dodaj pupila" /> }} />
      <Stack.Screen name="AddFriendScreen" component={AddFriendScreen} options={{ headerTitle: (props) => <LogoTitle {...props} fontSize={25} title="Dodaj przyjaciela" /> }}/>
      <Stack.Screen name="ChatScreen" component={ChatScreen} options={{ headerTitle: (props) => <LogoTitle {...props} fontSize={25} title="CHAT" /> }}/>
    </Stack.Navigator>
  );
};

export default StackNavigator;
