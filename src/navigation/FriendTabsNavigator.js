// FriendTabsNavigator.js
import React from 'react';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import FriendInfoScreen from '../screens/FriendInfoScreen';
import FriendMapScreen from '../screens/FriendMapScreen';
import colors from '../styles/colors';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';

const { Navigator, Screen } = createBottomTabNavigator();

const FriendTabsNavigator = ({ route }) => {
  const { petsId, friendId } = route.params;

  return (
    <Navigator screenOptions={{
      headerShown: false,


    }} >
      <Screen name="Informacje" component={FriendInfoScreen} initialParams={{ petsId, friendId }} options={{
        tabBarLabel: 'Info',
        tabBarIcon: ({ color, size }) => (
          <MaterialCommunityIcons name="information-outline" color={color} size={size} />
        ),
      }} />
      <Screen name="Mapa" component={FriendMapScreen} initialParams={{ petsId, friendId }} options={{
        tabBarLabel: 'Mapa',
        tabBarIcon: ({ color, size }) => (
          <MaterialCommunityIcons name="google-maps" color={color} size={size} />
        ),
      }} />
    </Navigator>
  );
};

export default FriendTabsNavigator;
