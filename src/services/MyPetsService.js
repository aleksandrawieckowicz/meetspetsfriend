const API_URL = 'http://10.0.2.2:3000';
// const API_URL = 'http://localhost:3000';

const MyPetsService = {
  getMyPets: async () => {
    try {
      const response = await fetch(`${API_URL}/myPets`);

      if (!response.ok) {
        throw new Error('Network response was not ok. Status: ${response.status}');
      }

      return await response.json();

    } catch (error) {
      throw new Error(`Error fetching data: ${error.message}`);
    }
  },

  getMyPetsById: async (petId) => {
    try {
      const response = await fetch(`${API_URL}/myPets/${petId}`);

      if (!response.ok) {
        throw new Error('Network response was not ok. Status: ${response.status}');
      }

      return await response.json();

    } catch (error) {
      throw new Error(`Error fetching data: ${error.message}`);
    }
  },

  postNewPet: async (newPetData) => {
    try {
      const response = await fetch(`${API_URL}/myPets`, {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify(newPetData),
      });

      if (!response.ok) {
        throw new Error(`HTTP error! Status: ${response.status}`);
      }

      const data = await response.json();
      console.log('Dodano nowego zwierzaka:', data);
    } catch (error) {
      console.error('Błąd:', error);
      throw error;
    }
  },

  actualizeInfo: async (petId, newPetData) => {
    try {
      const response = await fetch(`${API_URL}/myPets/${petId}`, {
        method: 'PUT',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify(newPetData),
      });

      if (!response.ok) {
        console.error('Błąd aktualizacji danych o zwierzaku:', response.status, response.statusText);
      }

      const data = await response.json();
      console.log('Zaktualizowano dane zwierzaka:', data);

    } catch (error) {
      console.error('Błąd:', error.message);
      throw error;
    }
  },

  updateMyLocation: async (petsId, newLocationX, newLocationY) => {
    try {
      const response = await fetch(`${API_URL}/myPets/${petsId}`, {
        method: 'PATCH',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({
          myLocationX: newLocationX,
          myLocationY: newLocationY,
        }),
      });

      if (!response.ok) {
        console.error('Błąd aktualizacji lokalizacji zwierzaka:', response.status, response.statusText);
      }

      const data = await response.json();
      console.log('Zaktualizowano lokalizację zwierzaka:', data);

    } catch (error) {
      console.error('Błąd:', error.message);
      throw error;
    }
  },

  updateMyLocationForAllPets: async (newLocationX, newLocationY) => {
    try {
      const petsData = await MyPetsService.getMyPets();

      console.log('petsData:', petsData);

      if (!petsData || petsData.length === 0) {
        console.error('Error: petsData is undefined or empty.');
        return;
      }

      const updatePromises = petsData.map(async (pet) => {
        const response = await fetch(`${API_URL}/myPets/${pet.id}`, {
          method: 'PATCH',
          headers: {
            'Content-Type': 'application/json',
          },
          body: JSON.stringify({
            myLocationX: newLocationX,
            myLocationY: newLocationY,
          }),
        });

        if (!response.ok) {
          console.error('Błąd aktualizacji lokalizacji zwierzaka:', response.status, response.statusText);
        }

        const data = await response.json();
        console.log('Zaktualizowano lokalizację zwierzaka:', data);
      });

      await Promise.all(updatePromises);
    } catch (error) {
      console.error('Błąd:', error.message);
      throw error;
    }
  },

  clearMyLocationForAllPets: async () => {
    try {
      const petsData = await MyPetsService.getMyPets();

      console.log('petsData:', petsData);

      if (!petsData || petsData.length === 0) {
        console.error('Error: petsData is undefined or empty.');
        return;
      }

      const clearPromises = petsData.map(async (pet) => {
        const response = await fetch(`${API_URL}/myPets/${pet.id}`, {
          method: 'PATCH',
          headers: {
            'Content-Type': 'application/json',
          },
          body: JSON.stringify({
            myLocationX: null,
            myLocationY: null,
          }),
        });

        if (!response.ok) {
          console.error('Błąd aktualizacji lokalizacji zwierzaka:', response.status, response.statusText);
        }

        const data = await response.json();
        console.log('Zaktualizowano lokalizację zwierzaka:', data);
      });

      await Promise.all(clearPromises);
    } catch (error) {
      console.error('Błąd:', error.message);
      throw error;
    }
  },
  patchFriendChat: async (petId, friendId, updatedFriendData) => {
    try {
      const response = await fetch(`${API_URL}/myPets/${petId}`, {
        method: 'PATCH',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({
          friends: updatedFriendData,
        }),
      });

      if (!response.ok) {
        console.error('Błąd aktualizacji danych o przyjacielu:', response.status, response.statusText);
      }

      const data = await response.json();
      console.log('Zaktualizowano dane przyjaciela:', data);

    } catch (error) {
      console.error('Błąd:', error.message);
      throw error;
    }
  },

};







export default MyPetsService;
