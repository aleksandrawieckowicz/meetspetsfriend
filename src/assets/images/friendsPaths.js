const friendsPaths = [
  { id: '1', source: require('../../assets/images/friendsImg/1.jpg') },
  { id: '2', source: require('../../assets/images/friendsImg/2.jpg') },
  { id: '3', source: require('../../assets/images/friendsImg/3.jpg') },
  { id: '4', source: require('../../assets/images/friendsImg/4.jpg') },
  { id: '5', source: require('../../assets/images/friendsImg/5.jpg') },
  { id: '6', source: require('../../assets/images/friendsImg/6.jpg') },
  { id: '7', source: require('../../assets/images/friendsImg/7.jpg') },
  { id: '8', source: require('../../assets/images/friendsImg/8.jpg') },
  { id: '9', source: require('../../assets/images/friendsImg/9.jpg') },
  { id: '10', source: require('../../assets/images/friendsImg/10.jpg') },
  ];
  
  export default friendsPaths;