const myPetsImagePaths = [
    { id: '0', source: require('../../assets/images/myPetsImg/0.jpg') },
    { id: '1', source: require('../../assets/images/myPetsImg/1.jpg') },
    { id: '2', source: require('../../assets/images/myPetsImg/2.jpg') },
    { id: '3', source: require('../../assets/images/myPetsImg/3.jpg') },
    { id: '4', source: require('../../assets/images/myPetsImg/4.jpg') },
    { id: '5', source: require('../../assets/images/myPetsImg/5.jpg') },
    { id: '6', source: require('../../assets/images/myPetsImg/6.jpg') },
    { id: '7', source: require('../../assets/images/myPetsImg/7.jpg') },
    { id: '8', source: require('../../assets/images/myPetsImg/8.jpg') },
    { id: '9', source: require('../../assets/images/myPetsImg/9.jpg') },
    { id: '10', source: require('../../assets/images/myPetsImg/10.jpg') },
  ];
  
  export default myPetsImagePaths;