const colors = {
    mainColor: '#A1C7A3',
    titleColor: '#DB542A',
    mainTextColor: '#5a5c5e',
    backgroundColor: '#e4e7e5',
    multiBoxColor: '#fcfffe',
    txtColor1: '#434a9c',
    btnColor: '#e25d01',
    header2Color: '#8ea1bf',
    normalTxt: '#3d403d',
    ligthGray: '#e4ebe7'
  };
  
  export default colors;