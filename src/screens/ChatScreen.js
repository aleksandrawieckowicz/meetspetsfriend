import React, { useEffect, useState, useRef } from 'react';
import { View, Text, StyleSheet, FlatList, Image, TextInput, TouchableOpacity } from 'react-native';
import MyPetsService from '../services/MyPetsService';
import moment from 'moment-timezone';

moment.tz.setDefault('Europe/Warsaw');

const ChatScreen = ({ route }) => {
    const { petsId, friendId } = route.params;
    const [onePetData, setOnePetData] = useState(null);
    const [messages, setMessages] = useState([]);
    const [newMessageText, setNewMessageText] = useState('');
    const flatListRef = useRef(null);

    useEffect(() => {
        const fetchData = async () => {
            try {
                const data = await MyPetsService.getMyPetsById(petsId);
                setOnePetData(data);
                setMessages(data?.friends?.[friendId - 1]?.chat || []);
            } catch (error) {
                console.log(error.myMessage);
            }
        };
        fetchData();
    }, [petsId, friendId]);

    const sendMessage = async () => {
        try {
            const petData = await MyPetsService.getMyPetsById(petsId);

            if (petData && petData.friends && petData.friends[friendId - 1]) {
                const newMessageObj = {
                    id: (petData.friends[friendId - 1].chat ? petData.friends[friendId - 1].chat.length + 1 : 1).toString(),
                    date: moment().format('HH-mm DD-MM-YYYY'),
                    author: 'me',
                    text: newMessageText || '',
                };

                const updatedFriendData = [...petData.friends];
                updatedFriendData[friendId - 1].chat.push(newMessageObj);

                await MyPetsService.patchFriendChat(petsId, friendId, updatedFriendData);

                setMessages([...messages, newMessageObj]); 
                setNewMessageText(''); 

                flatListRef.current.scrollToEnd({ animated: true });
            }
        } catch (error) {
            console.error('Błąd:', error.message);
        }
    };

    const renderMessage = ({ item }) => (
        <View style={[styles.message, item.author === 'me' ? styles.myMessage : styles.friendMessage]}>
            <Text>{item?.date}</Text>
            <Text style={[styles.messageTxt, item.author === 'me' ? styles.myMessageTxt : styles.friendMessageTxt]}>
                {item?.text}
            </Text>
        </View>
    );

    return (
        <View style={styles.container}>
            {onePetData ? (
                <>
                    <FlatList
                        ref={flatListRef}
                        style={styles.messagesBox}
                        data={messages}
                        keyExtractor={(item) => item.id.toString()}
                        renderItem={renderMessage}
                        onContentSizeChange={() => {
                            flatListRef.current.scrollToEnd({ animated: true });
                        }}
                    />
                    <View style={styles.newMsg}>
                        <TextInput
                            style={styles.input}
                            placeholder="Napisz..."
                            onChangeText={(text) => setNewMessageText(text)}
                            value={newMessageText}
                        />
                        <TouchableOpacity onPress={sendMessage}>
                            <Image source={require('../assets/icons/send.png')} style={styles.icon} />
                        </TouchableOpacity>
                    </View>
                </>
            ) : (
                <Text>Loading...</Text>
            )}
        </View>
    );
};

const styles = StyleSheet.create({
    container: {
        flex: 1,
        width: '100%',
        backgroundColor: '#eee',
    },
    messagesBox: {
        height: '80%',
        width: '100%',
    },
    message: {
        marginVertical: 4,
        width: '60%',
    },
    myMessage: {
        alignSelf: 'flex-end',
        marginRight: 5,
    },
    friendMessage: {
        alignSelf: 'flex-start',
        marginLeft: 5,
    },
    messageTxt: {
        borderRadius: 8,
        padding: 10,
    },
    myMessageTxt: {
        backgroundColor: '#add8e6',
    },
    friendMessageTxt: {
        backgroundColor: 'lightgray',
    },
    input: {
        height: 40,
        borderColor: 'gray',
        borderWidth: 1,
        margin: 10,
        padding: 8,
        display: 'flex',
        width: '80%',
    },
    icon: {
        height: 40,
        width: 40,
        display: 'flex',
    },
    newMsg: {
        display: 'flex',
        flexDirection: 'row',
        alignItems: 'center',
    },
});

export default ChatScreen;
