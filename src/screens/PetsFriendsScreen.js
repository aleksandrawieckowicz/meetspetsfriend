import React, { useEffect, useState } from "react";
import { View, StyleSheet, Button } from 'react-native';
import PetsFriendsList from "../components/molecules/PetsFriendsList";
import colors from "../styles/colors";
import ButtonCustom from "../components/atoms/ButtonCustom";

const PetsFriendsScreen = ({ route, navigation }) => {
    const { petsId } = route.params;

    const goToAddFriendScreen = () => {
        navigation.navigate('AddFriendScreen', { petId: petsId });
      };

    return (
        <View style={styles.container}>
            <PetsFriendsList style={styles.petsFriendsListStyle} petsId={petsId}/>
            <View style={styles.buttonContainer}>
            <ButtonCustom onPress={goToAddFriendScreen} title="DODAJ PRZYJACIELA" color={colors.btnColor} width={200} />
            </View>
        </View>
    );
};

const styles = StyleSheet.create({
    container: {
        display: 'flex',
        height: '100%',
        width: '100%',
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'center',
    },

    // headerStyle: {
    //     width: '100%',
    //     height: '5%',
    //     backgroundColor: 'skyblue',
    // },

    // myPetsListStyle: {
    //     height: '75%',
    // },
    petsFriendsListStyle: {
        height: '90%',
    },

    buttonContainer: {
        width: '100%',
        height: '13%',
        flexDirection: 'row',
        justifyContent: 'center',
        padding: 16,
        backgroundColor: colors.mainColor,
        marginTop: '30',
        alignItems: 'center'
    },
});


export default PetsFriendsScreen;
