// AllFriendMapScreen.js

import React, { useState, useEffect } from 'react';
import { View, Text, Button, StyleSheet } from 'react-native';
import MapView, { Marker } from 'react-native-maps';
import MyPetsService from '../services/MyPetsService';
import ButtonCustom from '../components/atoms/ButtonCustom';
import colors from '../styles/colors';

const AllFriendMapScreen = () => {
  const [addedMessage, setAddedMessage] = useState('');
  const [currentLocation, setCurrentLocation] = useState({
    latitude: 50.88,
    longitude: 20.637,
    latitudeDelta: 0.0922,
    longitudeDelta: 0.0421,
  });

  const handleUpdateAllLocations = async () => {
    try {
      await MyPetsService.updateMyLocationForAllPets(currentLocation.latitude, currentLocation.longitude);
      setAddedMessage('Wysłano lokalizację przyjaciołom!');
    } catch (error) {
      console.error('Błąd:', error.message);
    }
  };

  const handleFinishWalk = async () => {
    try {
      await MyPetsService.clearMyLocationForAllPets();
      setAddedMessage('Spacer zakończony!');
    } catch (error) {
      console.error('Błąd:', error.message);
    }
  };

  return (
    <View style={styles.container}>
      <MapView
        style={styles.map}
        initialRegion={{
          latitude: 50.88,
          longitude: 20.637,
          latitudeDelta: 0.05,
          longitudeDelta: 0.05,
        }}
      >
        <Marker
          coordinate={{
            latitude: currentLocation.latitude,
            longitude: currentLocation.longitude,
          }}
          title="Moja Lokalizacja"
          pinColor="blue"
        />

      </MapView>
      <View style={styles.bottomBox}>
        <View>
          <Text style={styles.txt}>{addedMessage}</Text>
        </View>
        <View  style={styles.btn}>
        <ButtonCustom onPress={handleUpdateAllLocations} title="Ogłoś spacer dla wszystkich" color={colors.btnColor} width={250}/>
        </View>
        <View  style={styles.btn}>
        <ButtonCustom onPress={handleFinishWalk} title="Zakończ spacer" color={colors.btnColor} width={180} />
        </View>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    ...StyleSheet.absoluteFillObject,
    justifyContent: 'flex-end',
    alignItems: 'center',
  },
  map: {
    ...StyleSheet.absoluteFillObject,
    height: '70%',
  },
  bottomBox: {
    height: '30%',
    justifyContent: 'center',
    alignItems: 'center'
  },
  message: {
    color: 'green',
    marginTop: 10,
  },
  btn: {
    marginTop: 15
  },
  txt: {
    color: colors.mainTextColor,
    fontWeight: 'bold',
    fontSize: 18,
  }
});

export default AllFriendMapScreen;
