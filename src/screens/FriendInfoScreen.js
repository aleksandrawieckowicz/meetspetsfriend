import React, { useEffect, useState } from "react";
import { View, StyleSheet, Text, Button } from 'react-native';
import PetsFriendsList from "../components/molecules/PetsFriendsList";
import MyPetsService from "../services/MyPetsService";
import FriendInfo from "../components/molecules/FriendInfo";
import ButtonCustom from "../components/atoms/ButtonCustom";
import colors from "../styles/colors";


const FriendInfoScreen = ({ route, navigation }) => {
    const { petsId, friendId } = route.params;

    const goToChatScreen = () => {
        navigation.navigate('ChatScreen', { petsId: petsId, friendId: friendId });
      };

    return (
        <View style={styles.container}>
            <FriendInfo petsId={petsId} friendId={friendId} style={styles.friendInfoStyle}></FriendInfo>
            <View style={styles.buttonContainer}>
            <ButtonCustom onPress={goToChatScreen} title="CHAT" color={colors.btnColor} width={120} />
            </View>
        </View>
    );
};

const styles = StyleSheet.create({
    container: {
        display: 'flex',
        height: '100%',
        width: '100%',
        alignItems: 'center',
        justifyContent: 'center',
    },

    icon: {
        height: 40,
        aspectRatio: 1,
      },

    friendInfoStyle: {
        height: '80%',
    },

    buttonContainer: {
        width: '100%',
        height: '35%',
        padding: 16,
        backgroundColor: colors.backgroundColor,
        marginBottom: 100,
        alignItems: 'center'
    },
});


export default FriendInfoScreen;
