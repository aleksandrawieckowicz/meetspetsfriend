import React from "react";
import { Text, View, StyleSheet, Button, Image } from "react-native";
import MyPetsList from "../components/molecules/MyPetsList";
import colors from "../styles/colors";
import ButtonCustom from "../components/atoms/ButtonCustom";

const HomeScreen = ({ navigation }) => {

    const goToAddPetScreen = () => {
        navigation.navigate('AddPetScreen');
      };

      const goToAllFriendMapScreen = () => {
        navigation.navigate('AllFriendMapScreen');
      };

    return (
        <View style={styles.container}>
            <View style={styles.headerStyle}>
                <Text style={styles.text}>WYBIERZ PUPILA</Text>
            </View>
            <MyPetsList style={styles.myPetsListStyle}></MyPetsList>
            <View style={styles.buttonContainer}>
            <ButtonCustom onPress={goToAllFriendMapScreen} title="OGŁOŚ SPACER" color={colors.btnColor} width={160} />
            <ButtonCustom onPress={goToAddPetScreen} title="DODAJ PUPILA" color={colors.btnColor} width={160} />
            </View>
        </View>
    );
};

const styles = StyleSheet.create({
    container: {
        height: '100%',
        width: '100%',
        backgroundColor: colors.backgroundColor,
        alignItems: 'center',
        justifyContent: 'center',
    },

    headerStyle: {
        flexDirection: 'row',
        width: '100%',
        height: '8%',
        backgroundColor: colors.backgroundColor,
        alignItems: 'center',
        justifyContent: 'center',
    },

    myPetsListStyle: {
        height: '83%',
    },

    buttonContainer: {
        width: '100%',
        height: '12%',
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        padding: 16,
        backgroundColor: colors.mainColor,
    },

    text: {
        color: colors.mainTextColor,
        fontWeight: 'bold',
        fontSize: 20,
    },

    icon: {
        height: 30,
      },

    btnStyle: {
        backgroundColor: colors.btnColor,
    }
});

export default HomeScreen;
