// AddFriendScreen.js
import React, { useState, useEffect } from 'react';
import { useNavigation } from '@react-navigation/native';
import { View, Text, StyleSheet, KeyboardAvoidingView, Platform } from 'react-native';
import MyPetsService from '../services/MyPetsService';
import NewFriendForm from '../components/molecules/NewFriendForm';
import colors from '../styles/colors';


const AddFriendScreen = ({ route }) => {
  const { petId } = route.params;
  const navigation = useNavigation();
  const [addedFriendMessage, setAddedFriendMessage] = useState('');

  const onReturn = (updatedPetData) => {
    setAddedFriendMessage('Nowy przyjaciel dodany!');
  };

  return (
    // <KeyboardAvoidingView
    //   style={{ flex: 1 }}
    //   behavior={Platform.OS === 'ios' ? 'padding' : 'height'}
    // >
      <View style={styles.container}>
        <NewFriendForm petId={petId} onReturn={onReturn} />
        {addedFriendMessage !== '' && <Text style={styles.message}>{addedFriendMessage}</Text>}
      </View>
    // </KeyboardAvoidingView >
  );
};

const styles = StyleSheet.create({
  container: {
    padding: 16,
  },
  message: {
    color: colors.mainTextColor,
    fontWeight: 'bold',
    fontSize: 18,
  },
});

export default AddFriendScreen;
