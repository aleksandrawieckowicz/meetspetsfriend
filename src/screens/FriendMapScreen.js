import React, { useState, useEffect } from 'react';
import { View, StyleSheet, Button, Text } from 'react-native';
import MapView, { Marker } from 'react-native-maps';
import MyPetsService from '../services/MyPetsService';
import colors from '../styles/colors';
import ButtonCustom from '../components/atoms/ButtonCustom';

const FriendMapScreen = ({ route }) => {
    const { petsId, friendId } = route.params;
    const currentLocation = { latitude: 50.88, longitude: 20.637 };
    const [friendLocation, setFriendLocation] = useState(null);
    const [addedMessage, setAddedMessage] = useState('');

    useEffect(() => {
        const fetchFriendLocation = async () => {
            try {
                const petData = await MyPetsService.getMyPetsById(petsId);
                const friend = petData.friends.find((friend) => friend.id === friendId);

                if (friend && friend.friendLocationX !== null && friend.friendLocationY !== null) {
                    setFriendLocation({
                        latitude: friend.friendLocationX,
                        longitude: friend.friendLocationY,
                    });
                }
            } catch (error) {
                console.error('Error fetching friend location:', error.message);
            }
        };

        fetchFriendLocation();
    }, [petsId, friendId]);

    const handleUpdateLocation = async () => {
        try {
            await MyPetsService.updateMyLocation(petsId, currentLocation.latitude, currentLocation.longitude);
            setAddedMessage('Jesteś na spacerze!');


        } catch (error) {
            console.error('Error updating location:', error.message);
        }
    };

    return (
        <View style={styles.container}>
            <MapView
                style={styles.map}
                initialRegion={{
                    latitude: friendLocation ? friendLocation.latitude : 50.88,
                    longitude: friendLocation ? friendLocation.longitude : 20.637,
                    latitudeDelta: 0.05,
                    longitudeDelta: 0.05,
                }}
            >
                <Marker
                    coordinate={{
                        latitude: currentLocation.latitude,
                        longitude: currentLocation.longitude,
                    }}
                    title="Moja Lokalizacja"
                    pinColor="blue"
                />

                {friendLocation && (
                    <Marker
                        coordinate={{
                            latitude: friendLocation.latitude,
                            longitude: friendLocation.longitude,
                        }}
                        title="Lokalizacja Przyjaciela"
                        pinColor="green"
                    />
                )}
            </MapView>
            <View style={styles.bottomBox}>
                <View style={styles.alertStyle}>
                    {friendLocation?.latitude == null && friendLocation?.longitude == null ? (
                        <Text style={styles.txt} >Przyjaciel nie jest na spacerze</Text>
                    ) : <Text style={styles.txt}>Przyjaciel jest na spacerze!</Text>}
                </View>
                <ButtonCustom onPress={handleUpdateLocation} title="OGŁOŚ SPACER" color={colors.btnColor} width={160} />
                <View style={styles.msgBox}>
                {addedMessage !== '' && <Text style={styles.message}>{addedMessage}</Text>}
                </View>
            </View>
        </View>
    );
};

const styles = StyleSheet.create({
    container: {
        ...StyleSheet.absoluteFillObject,
        justifyContent: 'flex-end',
        alignItems: 'center',
    },
    map: {
        ...StyleSheet.absoluteFillObject,
        height: '77%',
    },
    bottomBox: {
        height: '23%',
        justifyContent: 'center',
        alignItems: 'center'
    },
    message: {
        color: '#1400b4',
        marginTop: 10,
        fontWeight: 'bold',
        fontSize: 18,
    },
    msgBox: {
        height: 50,
    },

    txt: {
        marginTop: 15,
        color: '#0da417',
        fontWeight: 'bold',
        fontSize: 18,
        marginBottom: 15
    },
});

export default FriendMapScreen;
