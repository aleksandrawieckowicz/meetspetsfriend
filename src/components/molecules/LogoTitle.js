// StackNavigator.js
import React from 'react';
import { View, Text, Image } from 'react-native';
import { createStackNavigator } from '@react-navigation/stack';
import colors from '../../styles/colors';

const Stack = createStackNavigator();

function LogoTitle({ fontSize = 26, title = "Meet Pet's Friend" }) {
  return (
    <View style={{ flexDirection: 'row', width: '100%', alignItems: 'center' }}>
      <View style={{ width: '20%' }}>
        <Image
          style={{ width: 60, height: 60 }}
          source={require('../../assets/icons/logo.png')}
        />
      </View>
      <View style={{ width: '80%', justifyContent: 'center' }}>
        <Text style={{ fontWeight: 'bold', fontSize: fontSize, color: colors.titleColor, textAlign: 'center' }}>{title}</Text>
      </View>
    </View>
  );
}

export default LogoTitle;