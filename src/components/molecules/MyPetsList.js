import React, { useEffect, useState } from "react";
import { Text, View, StyleSheet, Image, ScrollView, TouchableOpacity } from "react-native"
import MyPetsService from "../../services/MyPetsService";
import myPetsImagePaths from "../../assets/images/myPetsPaths";
import { useNavigation, useIsFocused } from '@react-navigation/native';
import colors from "../../styles/colors";

const MyPetsList = () => {

  const [myPetsList, setMyPetsList] = useState([]);
  const navigation = useNavigation();
  const isFocused = useIsFocused(); // Hook do sprawdzenia, czy komponent jest aktualnie wyświetlany

  const handleViewPress = (key) => {
    console.log('Selected Key:', key);
    navigation.navigate('PetsFriendsScreen', { petsId: key });
  };

  useEffect(() => {
    const fetchData = async () => {
      try {
        const data = await MyPetsService.getMyPets();
        setMyPetsList(data);
      } catch (error) {
        console.error(error.message);
      }
    };
    fetchData();
  }, [isFocused]);


  const renderPetsList = () => {
    return myPetsList.map((item) => (<TouchableOpacity key={item?.id} onPress={() => handleViewPress(item?.id)} style={styles.onePetContainer} >
      <Image source={myPetsImagePaths[item?.id % myPetsImagePaths.length].source} style={styles.img} />
      <View style={styles.petsInfo}>
        <Text style={styles.txt1}>{item?.name}</Text>
        <Image source={require('../../assets/icons/cake.png')} style={styles.icon} />
        <Text style={styles.txt2}>{item?.month}</Text>
        <Text style={styles.txt2}>{item?.year}</Text>
      </View>
    </TouchableOpacity>))
  };

  return <ScrollView style={styles.container}>
    {renderPetsList()}
  </ScrollView>

}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    width: '100%',
    padding: 10,
    paddingTop: 0,
    backgroundColor: colors.backgroundColor,
  },

  img: {
    width: '50%', // Ustaw szerokość na 50% ekranu, aby uzyskać 2 kolumny
    height: 190,
    aspectRatio: 1, // Ustala proporcje zachowując kwadratowe obrazy
    margin: 0,
    borderRadius: 40,
  },

  onePetContainer: {
    width: '95%', // Ustaw szerokość na 50% ekranu, aby uzyskać 2 kolumny
    margin: 10,
    backgroundColor: colors.multiBoxColor,
    flexDirection: 'row',
    justifyContent: 'space-between',
    borderRadius: 40,
  },

  icon: {
    height: 40,
    aspectRatio: 1,
  },

  petsInfo: {
    width: '50%',
    alignItems: 'center',
    justifyContent: 'center',
  },

  txt1: {
    fontWeight: 'bold',
    fontSize: 20,
    fontFamily: 'Roboto',
    color: colors.mainTextColor
  },
  txt2: {
    fontWeight: 'bold',
    fontSize: 20,
    fontFamily: 'Roboto',
    color: colors.txtColor1
  }
});


export default MyPetsList;
