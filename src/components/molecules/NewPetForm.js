import React, { useState } from 'react';
import { View, TextInput, Button, StyleSheet, Text } from 'react-native';
import { Picker } from '@react-native-picker/picker';
import MyPetsService from '../../services/MyPetsService';
import { useNavigation } from '@react-navigation/native';
import ButtonCustom from '../atoms/ButtonCustom';
import colors from '../../styles/colors';



const NewPetForm = (props) => {

    const navigation = useNavigation();

    const [petData, setPetData] = useState({
        id: '',
        name: '',
        month: '1',
        year: '2000',
        sex: 'samiec',
        myLocationX: null,
        myLocationY: null,
        friends: []
    });

    const isInputValid = () => {
        return (
            petData.name.trim() !== ''
        );
    }
    const [addedMessage, setAddedMessage] = useState('');

    const addNewPet = async () => {
        try {
            if (!isInputValid()) {
                console.warn('Proszę wypełnić wszystkie pola.');
                return;
            } else {
                const existingPets = await MyPetsService.getMyPets();
                const newId = existingPets.length + 1;

                const newPetData = {
                    ...petData,
                    id: newId.toString(),
                    month: parseInt(petData.month),
                    year: parseInt(petData.year),
                };

                await MyPetsService.postNewPet(newPetData);
                setAddedMessage('Dodano nowego zwierzaka!');
                setTimeout(() => {
                    navigation.goBack();
                }, 1000);
            }
        } catch (error) {
            console.error('Błąd:', error);
        }
    };
    return (
        <View style={styles.container}>
            <View style={styles.inputBox}>
                <TextInput
                    style={styles.input}
                    placeholder="Imię zwierzaka"
                    value={petData.name}
                    onChangeText={(text) => setPetData({ ...petData, name: text })}
                />
                <Text style={styles.txt}>Wybierz miesiąc urodzin</Text>
                <Picker
                    style={styles.input}
                    selectedValue={petData.month}
                    onValueChange={(itemValue) => setPetData({ ...petData, month: itemValue.toString() })}
                >
                    {Array.from({ length: 12 }, (_, index) => index + 1).map((month) => (
                        <Picker.Item key={month} label={month.toString()} value={month.toString()} />
                    ))}
                </Picker>
                <Text style={styles.txt}>Wybierz rok urodzin</Text>
                <Picker
                    style={styles.input}
                    selectedValue={petData.year}
                    onValueChange={(itemValue) => setPetData({ ...petData, year: itemValue.toString() })}
                >
                    {Array.from({ length: 25 }, (_, index) => 2000 + index).map((year) => (
                        <Picker.Item key={year} label={year.toString()} value={year.toString()} />
                    ))}
                </Picker>
                <Text style={styles.txt}>Wybierz płeć</Text>
                <Picker
                    style={styles.input}
                    selectedValue={petData.sex}
                    onValueChange={(itemValue) => setPetData({ ...petData, sex: itemValue })}
                >
                    <Picker.Item label="Samiec" value="Samiec" />
                    <Picker.Item label="Samica" value="Samica" />
                </Picker>
                <View style={styles.btnBox}>
                    <ButtonCustom onPress={addNewPet} title="DODAJ PUPILA" color={colors.btnColor} width={180} style={styles.btn} />
                </View>
                {addedMessage !== '' && <Text style={styles.message}>{addedMessage}</Text>}
            </View>
        </View>
    );
};

const styles = StyleSheet.create({
    container: {
        paddingRight: 16,
        paddingLeft: 16,
        height: '80%',
        alignItems: 'centers'
    },
    inputBox: {
        alignItems: 'center'
    },
    input: {
        width: 220,
        height: 40,
        borderColor: 'gray',
        borderWidth: 1,
        marginBottom: 0,
        padding: 10,
        backgroundColor: colors.ligthGray
    },
    message: {
        color: 'green',
        marginTop: 10,
        fontSize: 18
    },
    txt: {
        marginTop: 15,
        color: colors.mainTextColor,
        fontWeight: 'bold',
        fontSize: 15,
        fontWeight: 'bold',
        marginBottom: 5,
        alignSelf: 'center'
    },
    btnBox: {
        alignSelf: 'center',
        marginBottom: 10
    }
});

export default NewPetForm;
