import React, { useState, useEffect } from 'react';
import { View, Text, StyleSheet, TextInput } from 'react-native';
import MyPetsService from '../../services/MyPetsService';
import { useNavigation } from '@react-navigation/native';
import ButtonCustom from '../atoms/ButtonCustom';
import colors from '../../styles/colors';

const NewFriendForm = ({ petId, onFriendAdded, onReturn }) => {
  const navigation = useNavigation();
  const [newFriendData, setNewFriendData] = useState({
    name: '',
    ownerName: '',
    ownerSurname: '',
    address: '',
    friendLocationX: null,
    friendLocationY: null,
    chat: []
  });
  const [addedFriendMessage, setAddedFriendMessage] = useState('');

  useEffect(() => {
    const fetchData = async () => {
      try {
        if (petId) {
          const petData = await MyPetsService.getMyPetsById(petId);
          setNewFriendData({
            name: '',
            ownerName: '',
            ownerSurname: '',
            address: '',
            friendLocationX: null,
            friendLocationY: null,
            chat: []
          });
        } else {
          setNewFriendData({
            name: '',
            ownerName: '',
            ownerSurname: '',
            address: '',
            friendLocationX: null,
            friendLocationY: null,
            chat: []
          });
        }
      } catch (error) {
        console.error(error.message);
      }
    };

    fetchData();
  }, [petId]);

  const isInputValid = () => {
    return (
      newFriendData.name.trim() !== '' &&
      newFriendData.ownerName.trim() !== '' &&
      newFriendData.ownerSurname.trim() !== '' &&
      newFriendData.address.trim() !== ''
    );
  };

  const addNewFriend = async () => {
    try {
      if (!isInputValid()) {
        console.warn('Proszę wypełnić wszystkie pola.');
        return;
      }

      const petData = await MyPetsService.getMyPetsById(petId);

      const newFriend = {
        id: (petData.friends ? petData.friends.length + 1 : 1).toString(),
        ...newFriendData,
      };

      const updatedPetData = {
        ...petData,
        friends: [...petData.friends, newFriend],
      };

      await MyPetsService.actualizeInfo(petId, updatedPetData);
      onReturn(updatedPetData);
      setAddedFriendMessage('Nowy przyjaciel dodany!');
      setTimeout(() => {
        onReturn(updatedPetData);
        navigation.goBack();
      }, 1000);
    } catch (error) {
      console.error('Błąd:', error.message);
    }
  };

  return (
    <View style={styles.container}>
      <View style={styles.inputBoxStyle}>
        <TextInput
          style={styles.input}
          placeholder="Imię przyjaciela"
          value={newFriendData.name}
          onChangeText={(text) => setNewFriendData({ ...newFriendData, name: text })}
        />
        <TextInput
          style={styles.input}
          placeholder="Imię właściciela"
          value={newFriendData.ownerName}
          onChangeText={(text) => setNewFriendData({ ...newFriendData, ownerName: text })}
        />
        <TextInput
          style={styles.input}
          placeholder="Nazwisko właściciela"
          value={newFriendData.ownerSurname}
          onChangeText={(text) => setNewFriendData({ ...newFriendData, ownerSurname: text })}
        />
        <TextInput
          style={styles.input}
          placeholder="Adres"
          value={newFriendData.address}
          onChangeText={(text) => setNewFriendData({ ...newFriendData, address: text })}
        />
      </View>
      <View style={styles.bottomBox}>
        <ButtonCustom onPress={addNewFriend} title="DODAJ PRZYJACIELA" color={colors.btnColor} width={220} style={styles.btn} />
        {addedFriendMessage !== '' && <Text style={styles.message}>{addedFriendMessage}</Text>}    
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    padding: 16,
    paddingTop: 0,
    height: '100%',
    width: '100%',
  },
  input: {
    height: 40,
    borderColor: 'gray',
    borderWidth: 1,
    marginBottom: 15,
    padding: 5,
  },
  message: {
    color: colors.mainTextColor,
    fontWeight: 'bold',
    fontSize: 18,
    marginTop: 15
  },
  bottomBox: {
    marginTop: 110,
    height: '20%',
    justifyContent: 'center',
    alignItems: 'center',  
  },
  inputBoxStyle: {
    paddingTop: 30,
    height: '33%',
  }
});

export default NewFriendForm;
