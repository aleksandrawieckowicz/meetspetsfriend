import React, { useEffect, useState } from "react";
import { View, Text, Image, StyleSheet } from 'react-native';
import MyPetsService from "../../services/MyPetsService";
import friendsPaths from "../../assets/images/friendsPaths";
import { ScrollView, TouchableOpacity } from "react-native-gesture-handler";
import { useNavigation } from '@react-navigation/native';
import colors from "../../styles/colors";

const FriendInfo = (props) => {
    const { petsId, friendId } = props;
    const [onePetData, setOnePetData] = useState(null); 

    useEffect(() => {
        const fetchData = async () => {
            try {
                const data = await MyPetsService.getMyPetsById(petsId);
                setOnePetData(data);
            } catch (error) {
                console.error(error.message);
            }
        };
        fetchData();
    }, [petsId, friendId]);

    console.log("Received key on PetsFriendsScreen:", petsId);

    return (
        <View style={styles.container}>
            {onePetData && onePetData.friends && onePetData.friends[friendId - 1] ? (
                <View style={styles.infoContainer}>
                    <View style={styles.mainInfo}>
                        <View>
                            <Image source={friendsPaths[friendId].source} style={styles.img} />
                        </View>
                        <Text style={[styles.txt, styles.nameTxt]}>{onePetData.friends[friendId - 1].name}</Text>
                    </View>
                    <View style={styles.ownerInfoStyle}>
                        <View>
                            <Image source={require('../../assets/icons/pet.png')} style={styles.icon} />
                        </View>
                        <Text style={styles.txt}>{onePetData.friends[friendId - 1].ownerName}</Text>
                        <Text style={styles.txt}>{' '}</Text>
                        <Text style={styles.txt}>{onePetData.friends[friendId - 1].ownerSurname}</Text>

                    </View>
                    <View style={styles.addresStyle}>
                        <View>
                            <Image source={require('../../assets/icons/house.png')} style={styles.icon} />
                        </View>
                        <Text style={styles.txt}>{onePetData.friends[friendId - 1].address}</Text>
                    </View>
                </View>
            ) : (
                <Text>Loading...</Text>
            )}
        </View>
    );

};

const styles = StyleSheet.create({
    container: {
        marginTop: 130,
        display: 'flex',
        height: '100%',
        width: '100%',
        backgroundColor: colors.backgroundColor,
        alignItems: 'center',
    },

    infoContainer: {
        display: 'flex',
        height: '100%',
        width: '100%',
        alignItems: 'center',
        flexDirection: 'column'
    },

    img: {
        width: '50%', // Ustaw szerokość na 50% ekranu, aby uzyskać 2 kolumny
        height: 220,
        aspectRatio: 1, // Ustala proporcje zachowując kwadratowe obrazy
        margin: 25,
        borderRadius: 110,
    },
    icon: {
        height: 60,
        aspectRatio: 1,
        margin: 15
    },
    mainInfo: {
        marginTop: 110,
        width: '100%',
        height: '50%',
        alignItems: 'center',
        justifyContent: 'center',
        // backgroundColor: 'red'
    },
    ownerInfoStyle: {
        width: '100%',
        height: '12%',
        flexDirection: 'row',
        alignItems: 'center',
        marginTop: '60',
        // backgroundColor: 'green'
    },

    addresStyle: {
        width: '100%',
        height: '12%',
        flexDirection: 'row',
        alignItems: 'center',
        // backgroundColor: 'yellow'
    },

    txt: {
        fontWeight: 'bold',
        fontSize: 18,
        fontFamily: 'Roboto',
        color: colors.normalTxt,
        
      },

      nameTxt: {
        fontSize: 24,
      },
});


export default FriendInfo;
