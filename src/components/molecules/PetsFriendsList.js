import React, { useEffect, useState } from "react";
import { View, Text, Image, StyleSheet } from 'react-native';
import MyPetsService from "../../services/MyPetsService";
import friendsPaths from "../../assets/images/friendsPaths";
import { ScrollView, TouchableOpacity } from "react-native-gesture-handler";
import { useNavigation } from '@react-navigation/native';
import colors from "../../styles/colors";


const PetsFriendsList = (props) => {

    const FriendTabsNavigator = require("../../navigation/FriendTabsNavigator").default;

    const { petsId } = props;
    const navigation = useNavigation();
    const [onePetData, setOnePetData] = useState(null);

    const handleViewPress = (key) => {
        console.log('Selected Key:', key);
        navigation.navigate('FriendTabsNavigator', { petsId: petsId, friendId: key });
    };

    useEffect(() => {
        const fetchData = async () => {
            try {
                const data = await MyPetsService.getMyPetsById(petsId);
                setOnePetData(data);
            } catch (error) {
                console.error(error.message);
            }
        };
        fetchData();
    }, [petsId]);

    console.log("Received key on PetsFriendsScreen:", petsId);

    const renderList = () => {
        const friendsList = onePetData.friends;

        if (friendsList && friendsList.length > 0) {
            return (<ScrollView style={styles.friendsContainer}>
                {onePetData.friends.map((item, index) => (
                    <View key={item?.id} style={[
                        styles.onePetContainer,
                        index === 0 ? null : (item?.id % 2 === 0 ? styles.onePetContainerRight : styles.onePetContainerLeft),
                    ]}>
                        <TouchableOpacity key={item?.id} onPress={() => handleViewPress(item?.id)}>
                            {item?.friendLocationX != null ?
                                <Image source={require('../../assets/icons/alarm.png')} style={styles.iconAlert} /> : null}
                            <Image source={friendsPaths[item?.id % friendsPaths.length].source} style={styles.img} />
                        </TouchableOpacity>
                        <Text style={styles.txt}>{item?.name}</Text>
                    </View>
                ))}
            </ScrollView>);
        } else {
            return (
                <View style={styles.noFriendsBox}>
                    <View>
                         <Image source={require('../../assets/icons/sad.png')} style={styles.icon} />
                    <View style={styles.txtContainer}>
                        <Text style={styles.txt}>Brak przyjaciół</Text>
                    </View>

                </View>
                </View>)
        }
    }


    return (
        <View style={styles.container}>
            {onePetData ? (
                renderList()
            ) : (
                <Text style={styles.txt}>Loading...</Text>
            )}
        </View>
    );
};

const styles = StyleSheet.create({
    container: {
        flex: 1,
        width: '100%',
        alignItems: 'center',
        position: 'relative',

    },

    friendsContainer: {
        flex: 1,
        width: '100%',
        padding: 10,
        backgroundColor: colors.backgroundColor,
        //   display: 'flex',
        position: 'relative',

    },

    img: {
        width: '50%', // Ustaw szerokość na 50% ekranu, aby uzyskać 2 kolumny
        height: 190,
        aspectRatio: 1, // Ustala proporcje zachowując kwadratowe obrazy
        margin: 0,
        borderRadius: 100,
        resizeMode: 'cover',
    },

    onePetContainer: {
        display: 'flex',
        width: '100%',
        marginTop: 0,
        // backgroundColor: '#EBEBEB',
        flexDirection: 'column',
        justifyContent: 'center',
        position: 'relative',

    },

    onePetContainerLeft: {
        marginTop: -60,
        alignItems: 'flex-start',
    },

    onePetContainerRight: {
        marginTop: -60,
        alignItems: 'flex-end',
    },

    icon: {
        height: '100%',
        aspectRatio: 1,
        marginBottom: 20
    },

    iconAlert: {
        height: 30,
        aspectRatio: 1,
        resizeMode: 'cover',
        left: 5,
        top: 15,



    },

    txt: {
        fontWeight: 'bold',
        fontSize: 21,
        fontFamily: 'Roboto',
        color: colors.mainTextColor
    },

    alert: {
        justifyContent: 'center',
        alignItems: 'center'
    },

    noFriendsBox: {
        marginTop: 90,
        justifyContent: 'center',
        alignItems: 'center',
        height: '20%',
        aspectRatio: 1
    },
    txtContainer: {
        marginLeft: 30
    }
});



export default PetsFriendsList;
